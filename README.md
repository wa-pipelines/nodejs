# GitLab CI template for Node.js

This project implements a GitLab CI/CD template to build, test and analyse your
JavaScript/TypeScript/[Node.js](https://nodejs.org/) projects.

More precisely, it can be used by all projects based on [npm](https://www.npmjs.com/),
[yarn](https://yarnpkg.com/) or [pnpm](https://pnpm.io/) package managers.

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration)
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/wa-pipelines/nodejs/gitlab-ci-node@1.0.2
    inputs:
      image: "registry.hub.docker.com/library/node:20" # this is only an example
      lint-enabled: "true"
```
